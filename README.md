SymPA is an unsolvability planner, which aims at proving that a given planning task is
unsolvable.

Publication: Alvaro Torralba, SymPA: Symbolic Perimeter Abstractions for Proving
Unsolvability, Proceedings of the 1st Unsolvability International Planning Competition
(IPC 2016), at ICAPS'16, London, UK, 2016. (PDF:
https://people.cs.aau.dk/~alto/papers/ipc16.pdf)


# Installation

Use ./build.py script to compile

You may need to manually change the automake version installed in your machine. The
current default is 1.16 (see am__api_version variable in src/search/cudd-3.0.0/configure).


# Usage


./fast-downward.py <instance> --search <configuration>

For default behaviour you may run the code with the option --search "sympa", easy no? :D


Well, actually, there are tons of parameters (You can check the full list in
search/symbolic/sympa.cc, sym_params_search.cc, and sym_state_space_manager.cc). Some of
the most useful ones are:
 * debug=true -> Shows some information every time the symbolic search expands a set of states. It's useful to see what the planner is doing, so I recommend using it.
 * max_step_nodes=infinity -> Ensures that there are no abstract searches,
 * max_step_nodes=10000 -> Performs a lot of abstract searches (useful to test if your proofs are working with abstract searches in small instances because otherwise abstract searches will only be used in big instances that are not solvable without abstract searches).
* search_dir=fw, or search_dir=bw: the searches are only done in forward or backward direction (by default both are used).
* mutex_type=MUTEX_NOT: Disables the use of mutex information (important if you use backward search even if you disable h2, because the FD translator already provides some mutexes).
* perimeter_pdbs=false: Disables using perimeter pdbs to initialize the abstract search. This should only be relevant if abstract searches are used.

# License

SymPA is based on Fast Downward (for documentation and contact information see
http://www.fast-downward.org/)

The following directories are not part of Fast Downward as covered by this
license:
* ./src/search/ext
* ./src/search/cudd-3.0.0 (CUDD BDD library)

For the rest, the following license applies:

```
Copyright (C) 2003-2016 Malte Helmert
Copyright (C) 2008-2016 Gabriele Roeger
Copyright (C) 2010-2016 Jendrik Seipp
Copyright (C) 2010, 2011, 2013-2016 Silvan Sievers
Copyright (C) 2012-2016 Florian Pommerening
Copyright (C) 2016 Martin Wehrle
Copyright (C) 2013, 2015 Salome Simon
Copyright (C) 2014, 2015 Patrick von Reth
Copyright (C) 2015 Manuel Heusner, Thomas Keller
Copyright (C) 2009-2014 Erez Karpas
Copyright (C) 2014 Robert P. Goldman
Copyright (C) 2010-2012 Andrew Coles
Copyright (C) 2010, 2012 Patrik Haslum
Copyright (C) 2003-2011 Silvia Richter
Copyright (C) 2009-2011 Emil Keyder
Copyright (C) 2010, 2011 Moritz Gronbach, Manuela Ortlieb
Copyright (C) 2011 Vidal Alcázar Saiz, Michael Katz, Raz Nissim
Copyright (C) 2010 Moritz Goebelbecker
Copyright (C) 2007-2009 Matthias Westphal
Copyright (C) 2009 Christian Muise

Fast Downward is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Fast Downward is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
```
