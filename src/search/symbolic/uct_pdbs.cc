#include "uct_pdbs.h"

#include "original_state_space.h"
#include "breadth_first_search.h"
#include "sym_pdb.h"

#include "../operator_cost_function.h"
#include "../globals.h"
#include "../utils/debug_macros.h"
#include "../causal_graph.h"
#include "../utils/rng.h"


#include <algorithm>

using namespace std;

namespace symbolic {

UCTTree::UCTTree (SymVariables * vars_, const SymParamsMgr & mgrParams, OperatorCost cost_type) : vars(vars_) {	
    nodes.push_back(make_unique<UCTNode>(this, 0, mgrParams, cost_type));
    }


UCTNode * UCTTree::getUCTNode (const std::set<int> & pattern) {
	DEBUG_MSG(cout << "UCTTree::getUCTNode" << endl;);
	
    if (nodesByPattern.count(pattern)) {
	return nodesByPattern[pattern];
    }
    
    nodes.push_back(make_unique<UCTNode> (this, nodes.size(), pattern));
    UCTNode * newNode = nodes.back().get();
    
    nodesByPattern[pattern] = newNode;

    return newNode;
}


    UCTNode::UCTNode(UCTTree * tree_, int id_, const SymParamsMgr & mgrParams, OperatorCost cost_type) : 
	tree (tree_), id(id_), mgr(make_shared<OriginalStateSpace>(tree->get_vars(), mgrParams, make_shared<OperatorCostConstant>(cost_type))) {
    mgr->init();
    assert (pattern.size() <= g_variable_domain.size());

    for (size_t i = 0; i < g_variable_domain.size(); ++i) pattern.insert((int)i);
   
    assert (pattern.size() <= g_variable_domain.size());
}

    UCTNode::UCTNode(UCTTree * tree_, int id_, const std::set<int> & pattern_) : 
	tree(tree_), id(id_),pattern (pattern_),   mgr(nullptr) { 

    assert (pattern.size()<= g_variable_domain.size());

}

void UCTNode::init(std::shared_ptr<SymStateSpaceManager> parent, AbsTRsStrategy absTRsStrategy) {  
    mgr = make_shared<SymPDB> (parent, absTRsStrategy, pattern);
}

BreadthFirstSearch * UCTNode::initSearch(bool fw, const SymParamsSearch & searchParams) {
    if (fw) {
	assert(!fw_search);
	fw_search.reset(new BreadthFirstSearch (searchParams));
	fw_search->init(mgr, fw);
	return fw_search.get();
    } else {
	assert(!bw_search);
	bw_search.reset(new BreadthFirstSearch (searchParams));
	bw_search->init(mgr, fw);
	return bw_search.get();
    }
}


void UCTNode::refine_pattern (const set<int> & pattern, vector<pair<int, set<int>>> & patterns, int relaxed_variable) const {    
    vector<set<int>> patterns_goal(pattern.size());
    vector<bool> pattern_goal_redundant (pattern.size(), false);
    vector<int> pattern_var (g_variable_domain.size(), -1);
    const CausalGraph & causal_graph = get_causal_graph(g_root_task().get());

    int g = -1;
    for (int v : pattern) {
	g++;
	vector<int> open;
	if (pattern_var[v] == -1) {
	    pattern_var[v] = g;
	    open.push_back(v);
	    patterns_goal[g].insert(v);
	} else {
	    int p = pattern_var[v];
	    pattern_goal_redundant[p] = true;
	    for (int vo : patterns_goal[p]) {
		pattern_var[vo] = g;
		patterns_goal[g].insert(vo);
	    }
	}

	for (size_t i = 0; i < open.size(); ++i) { 
for (int vpre : causal_graph.get_eff_to_pre(open[i])) {
		if(!pattern.count(vpre)) continue;
		if (pattern_var[vpre] == -1) {
		    pattern_var[vpre] = g;
		    open.push_back(vpre);
		    patterns_goal[g].insert(vpre);
		} else if(pattern_var[vpre] != g) {
		    int p = pattern_var[vpre];
		    pattern_goal_redundant[p] = true;
		    for (int vo : patterns_goal[p]) {
			pattern_var[vo] = g;
			patterns_goal[g].insert(vo);
		    }
		}
		
	    }
	}
	if (patterns_goal[g].size() == pattern.size()) {
	    if (find_if(begin(patterns), end(patterns), [&](const pair<int, set<int>> & p1){
			return isSubset(patterns_goal[g], p1.second);}) == end(patterns)){
		patterns.erase(remove_if(begin(patterns), end(patterns), 
					 [&](const pair<int, set<int>>  & p1){return isSubset(p1.second, patterns_goal[g]);
					 }), end(patterns));
		patterns.push_back(pair<int, set<int>>(relaxed_variable, std::move(patterns_goal[g])));
	    }

	    return;
	}
    }

    for (size_t g = 0; g < pattern.size(); ++g) {
	if (!pattern_goal_redundant[g] && find_if(begin(patterns),
						  end(patterns),
						  [&](const pair<int, set<int>>  & p1){
						      return isSubset(patterns_goal[g], p1.second);}) == end(patterns)){
	    patterns.erase(remove_if(begin(patterns), end(patterns), 
				     [&](const pair<int, set<int>> & p1){return isSubset(p1.second, patterns_goal[g]);
				     }), end(patterns));

	    patterns.push_back(pair<int, set<int>>(relaxed_variable, std::move(patterns_goal[g])));
	}
    }
    
}


bool UCTNode::isSubset (const set<int> & p1, const set<int> & p2) const {
    vector<int> res;
    std::set_difference(p1.begin(), p1.end(), p2.begin(), p2.end(), std::inserter(res, res.end()));
    return res.empty();
}
bool UCTNode::existsFinishedSuperset(const set<int> & pat, pair<bool, bool> & redundant, std::set<UCTNode *> & cache) {
    if(cache.count(this)) return false;

    cache.insert(this);

    if (isSubset(pat, pattern)) {
	    
	if (fw_search && fw_search->finished()) redundant.first = true;
	if (bw_search && bw_search->finished()) redundant.second = true;
	if (redundant.first && redundant.second) return true;

	for (auto & c : children) {
	    if(c->existsFinishedSuperset(pat, redundant, cache)) return true;
	}
    } 

    return false;
}

void UCTNode::removeSubsets(const set<int> & pat, bool fw, set<UCTNode *> & cache) {
    if(cache.count(this)) return;
    cache.insert(this);
    if (isSubset(pattern, pat)) {
	if (fw && fw_search) {
	    info_fw.redundant = true;
	    //fw_search.reset(nullptr);
	}
	if (!fw && bw_search) {
	    info_bw.redundant = true;
	    //bw_search.reset(nullptr);
	}
    } 

    for (auto & c : children) {
	c->removeSubsets(pat, fw, cache);
	if (c->info_fw.redundant) children_fw.erase(find(begin(children_fw), end(children_fw), c), end(children_fw));
	if (c->info_bw.redundant) children_bw.erase(find(begin(children_bw), end(children_bw), c), end(children_bw));
    }
    
}


void UCTNode::initChildren ()  {
    if (!children.empty()) return;
    
    vector<pair<int, set<int> > > patterns;
    for (int v : pattern) {
	set<int> new_pattern (pattern);
	new_pattern.erase(std::find(begin(new_pattern), end(new_pattern), v));

	refine_pattern (new_pattern, patterns, v);
    }

    for (const auto & pp : patterns) {
	int v = pp.first;
	const auto & p = pp.second;
	pair<bool, bool> redundant (false, false);
	
	if(tree->getRoot()->existsFinishedSuperset(p, redundant)) continue;
		
	auto newChild = tree->getUCTNode(p);
	
	if(newChild) {
	    if(redundant.first) newChild->info_fw.redundant = true;
	    if(redundant.second) newChild->info_bw.redundant = true;

	    if (find(begin(children), 
		     end(children), newChild)  == end(children)) {
		children.push_back(newChild);
		//cout << "   " << *newChild << endl;
		children_fw.push_back(newChild);
		children_bw.push_back(newChild);
		children_var[newChild] = v;
	    }	       
	}
    }
    DEBUG_MSG(cout << "End init Children" << endl;);

}

std::ostream & operator<<(std::ostream &os, const UCTTree & tree){
    UCTNode * root  = tree.getRoot();

    os << "UCTTree (" << tree.nodes.size() << ") " << endl;
    root->print_tree(os, true) << endl;
    root->print_tree(os, false) << endl;

    return os;
}

    std::ostream & UCTNode::print_tree (std::ostream &os, bool fw) const {
	set <int> printed;
	print_tree(os, fw, printed);
	return os;
    }

    void UCTNode::print_tree (std::ostream &os, bool fw, std::set<int> & printed) const {
	if(printed.count(id)) return;
	os << "node" << id << "[label=\"" << get_info(fw).visits << "-" <<  get_info(fw).reward << "\"];" << endl;

	if(get_info(fw).visits > 1) {
	    const auto & children_list = (fw ? children_fw : children_bw);

	    for (auto c : children_list) {
		if (!c->isExplored(fw)) continue;
		os <<  "node" << id << " -> node" << c->id << ";" << endl;
		c->print_tree (os, fw); 
	    }
	}
    }
	


std::ostream & operator<<(std::ostream &os, const UCTNode & node){
    os << "PDB (" << node.pattern.size() << "): ";
    for (int v : node.pattern){
	os << v << " ";
    }
    return os;
}

UCTNode * UCTNode::getRandomChild (bool fw)  {    
    const auto & children_list = (fw ? children_fw : children_bw);
    if(children_list.empty()) return nullptr;

    return *g_rng()->choose(children_list); 	
}


double UCTNode::get_rave_value(int var, double UCT_C) const {
    return rave_reward[var] + UCT_C*sqrt(log(total_rave_visits)/(rave_visits[var] + 1)); 
}

UCTNode * UCTNode::getChild (bool fw, double UCT_C, double RAVE_K, UCTNode * root) const { 
    const auto & children_list = (fw ? children_fw : children_bw);
    if(children_list.empty()) return nullptr;
    vector<UCTNode *> best_children;
   
    if(!root->rave_reward.empty()) {
	double best_value = numeric_limits<double>::lowest();	 
	for(auto c : children_list) {
	    if(c->isExplored(fw)) continue;
	    double c_val = root->get_rave_value(children_var.at(c), UCT_C);
	    DEBUG_MSG(cout << "  Rave value " <<  children_var.at(c) << " is " << c_val ;);
	    if(best_value <= c_val) {
		if(best_value < c_val){
		    best_value = c_val;
		    best_children.clear();
		}

		best_children.push_back(c);
	    }
	}
	DEBUG_MSG(cout << endl;);
    } else {
	for(auto c : children_list) {
	    if(!c->isExplored(fw)) {
		best_children.push_back(c);
	    }
	}
    }

    
    if(!best_children.empty()) {
	return  *g_rng()->choose(best_children); 
    }	

    
    int total_visits = 0;
    for(auto c : children_list) {
	total_visits += c->get_info(fw).visits;
    } 

    double best_value = numeric_limits<double>::lowest();
    for(auto c : children_list) {
	double c_val = 0;
	if(!rave_reward.empty()){
    
	    double RAVE_Q = rave_reward[children_var.at(c)];
	    double RAVE_B = sqrt(RAVE_K/(3*total_visits + RAVE_K));
    
	    c_val = c->uct_value(fw, total_visits, UCT_C, RAVE_B) + RAVE_B*RAVE_Q;
    
	}else{
	    c_val = c->uct_value(fw, total_visits, UCT_C, 0);
	}
	if(best_value <= c_val) {
	    if(best_value < c_val){
		best_value = c_val;
		best_children.clear();
	    }

	    best_children.push_back(c);
	}
    }
    assert(!best_children.empty());
	    
    return *g_rng()->choose(best_children); 
} 


    double UCTNode::uct_value(bool fw, int visits_parent, double UCT_C, double RAVE_B) const {
	return get_info(fw).uct_value(visits_parent, UCT_C, RAVE_B);
    }


BreadthFirstSearch * UCTNode::relax(std::shared_ptr<BreadthFirstSearch> search, 
				       const SymParamsSearch & searchParams,
				       int maxRelaxTime, int maxRelaxNodes, 
				       double ratioRelaxTime, double ratioRelaxNodes, 
				       bool /*perimeterPDBs*/) {
    bool fw = search->isFW();
    assert(!getSearch(fw));
    
    shared_ptr<BreadthFirstSearch> newSearch = make_shared<BreadthFirstSearch>(searchParams);
    newSearch->init(search, mgr, maxRelaxTime, maxRelaxNodes);
    
    if (newSearch->nextStepNodes() < ratioRelaxNodes*search->nextStepNodes() && 
	newSearch->nextStepTime() < ratioRelaxTime*search->nextStepTime()) {
	// cout << " Relaxed and searchable " << newSearch->nextStepNodes() << ", " << newSearch->nextStepTime() << 
	//     " instead of " << search->nextStepNodes()  << " and"  << search->nextStepTime()  << endl;
	if (fw) fw_search = std::move(newSearch);
	else bw_search = std::move(newSearch); 
    } else {
	// cout << " Relaxed but not searchable " << newSearch->nextStepNodes() << ", " << newSearch->nextStepTime() << 
	//     " instead of " << search->nextStepNodes()  << " and"  << search->nextStepTime()  << endl;

    }

    return getSearch(fw);
}


void UCTNode::propagateNewDeadEnds(BDD bdd, bool isFW) {
    DEBUG_MSG(cout << " UCTNode::propagateNewDeadEnds" << endl;);
    
    if(mgr && (((isFW || mgr->isAbstracted()) && fw_search) || ((!isFW || mgr->isAbstracted()) && bw_search))){ 
	if (mgr->isAbstracted()){
	    bdd = mgr->shrinkForall(bdd);
	    if(!bdd.IsZero()) mgr->addDeadEndStates(!isFW, bdd);
	    
	}
	if(!bdd.IsZero()){
	    mgr->addDeadEndStates(!isFW, bdd);

	    if((isFW || mgr->isAbstracted()) && fw_search) {
		fw_search->notifyMutexes (bdd);
	    } 

	    if((!isFW || mgr->isAbstracted()) && bw_search) {
		bw_search->notifyMutexes (bdd);
	    } 
	}
    }
        
    for(auto c : children) {
	if (c) c->propagateNewDeadEnds(bdd, isFW);
    }    
    
}

bool UCTNode::chooseDirection(double UCT_C) const {
    if(!info_fw.explored() && !info_bw.explored()) return g_rng()->random_bool();
    if(!info_bw.explored()) return false;
    if(!info_fw.explored()) return true;
    
    double rfw = info_fw.uct_value(info_fw.visits + info_bw.visits, UCT_C); 
    double rbw = info_bw.uct_value(info_fw.visits + info_bw.visits, UCT_C);

    DEBUG_MSG(cout << "Choosing " << info_fw.visits << " " << info_bw.visits << " " << rfw << " " <<  rbw << endl;);
    if (rfw == rbw) return g_rng()->random_bool();
    return rfw > rbw;
}


void UCTNode::notifyReward (bool fw, double new_reward) {
    get_info(fw).update_reward(new_reward);
}


void UCTNode::notifyRewardRAVE (bool fw, double new_reward, const std::set<int> & final_pattern) {
    //cout << "Notify Reward RAVE" << endl;
    notifyReward(fw, new_reward);

    if(rave_reward.empty()) {
	rave_reward.resize(g_variable_domain.size(), 0.0);
	rave_visits.resize(g_variable_domain.size(), 0);
	total_rave_visits = 0;
    }

    //Apply RAVE
    for (int v : pattern) {
	if(std::find (begin(final_pattern), end(final_pattern), v) != std::end(final_pattern))
	    continue;
	//vector<double> & rave_reward = (fw? rave_reward_fw : rave_reward_bw);
	//vector<int> & rave_visits = (fw? rave_visits_fw : rave_visits_bw);
    
	rave_reward[v] =  (rave_reward[v]*rave_visits[v] + new_reward) / (rave_visits[v]+1);
	rave_visits[v] ++;
	total_rave_visits++;
    }    
}


}
